{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty
import Lucid
import View
import Model
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.IO as L

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    get "/" $ html $ renderText $ view model
    get "/home" $ html $ renderText $ home
    get "/gallery" $ html $ renderText $ gallery model
    get "/add" $ html $ renderText $ add
    post "/add/$_POST" $ do
        name <- param "name" `rescue` (\_ -> return "")
        url <- param "url" `rescue` (\_ -> return "")
        text $ TL.concat [name, "  ", url]
