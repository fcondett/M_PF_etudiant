{-# LANGUAGE OverloadedStrings #-}

module Model where

import Data.Text.Lazy
import Lucid

data Image = Image{
    nom :: Text,
    lien :: Text 
}

model :: [Image]
model =  Image "logo haskell" "https://www.haskell.org/img/haskell-logo.svg"
 : Image "Haskell Curry" "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/HaskellBCurry.jpg/260px-HaskellBCurry.jpg"
 : []