{-# LANGUAGE OverloadedStrings #-}

module View where

import qualified Data.Text.Lazy.IO as L
import qualified Data.Text.Lazy as L
import Lucid
import Model

view :: [Image] -> Html ()
view images = 
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            link_ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"]
        body_ $ div_ [class_ "container"] $ do
            div_ [class_ "row"] $ do
                div_ [class_ "col-1"] $ do 
                    h5_ "MyGallery"
                div_ [class_ "col-1"] $ do 
                    a_ [href_ "/home"] $ "home"
                div_ [class_ "col-1"] $ do 
                    a_ [href_ "/gallery"] $ "gallery"
                div_ [class_ "col-1"] $ do 
                    a_ [href_ "/add"] $ "add"
            div_ [class_ "row"] $ do
                home
            div_ [class_ "row"] $ do
                    gallery images
            div_ [class_ "row"] $ do
                    add


home :: Html ()
home =
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            link_ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"]
        body_ $ div_ [class_ "container"] $ do
            div_ [class_ "row"] $ do
                div_ [class_ "col-4"] $ do 
                    h3_ "Home"
            div_ [class_ "row"] $ do
                "Welcome. This is my gallery."
            div_ [class_ "row"] $ do 
                div_ [class_ "col-4 offset-8"] $ do
                        a_ [href_ "/#"] $ "top"
            hr_ []

gallery :: [Image] -> Html ()
gallery images =
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            link_ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"]
        body_ $ div_ [class_ "container flex-wrap"] $ do
            div_ [class_ "row"] $ do
                div_ [class_ "col-4"] $ do 
                    h3_ "Gallery"
            div_ [class_ "d-flex flex-wrap"] $ do
                div_ [class_ "p-2 bd-highlight"] $ do
                    figure_ $ do
                        img_ [src_ (L.toStrict (lien (head images))), class_ "img-fluid"]
                        figcaption_ (toHtml (nom (head images)))
                let images2 = tail images
                div_ [class_ "p-2 bd-highlight"] $ do
                    figure_ $ do
                        img_ [src_ (L.toStrict (lien (head images2))), class_ "img-fluid"]
                        figcaption_ (toHtml (nom (head images2)))

            div_ [class_ "row"] $ do 
                div_ [class_ "col-4 offset-8"] $ do
                    a_ [href_ "/#"] $ "top"
            hr_ []

add :: Html ()
add =
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
            link_ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"]
        body_ $ div_ [class_ "container"] $ do
            div_ [class_ "row"] $ do
                div_ [class_ "col-4"] $ do 
                    h3_ "Add"
            div_ [class_ "row"] $ do
                div_ [class_ "form-row"] $ do
                    div_ [class_ "form-group"] $ do
                        label_ [for_ "name"] "Name:"
                        input_ [id_ "name", class_ "form-control"]
            div_ [class_ "row"] $ do
                div_ [class_ "form-row"] $ do
                    div_ [class_ "form-group"] $ do
                        label_ [for_ "url"] "Url:"
                        input_ [id_ "url", class_ "form-control"]
            div_ [class_ "row"] $ do
                button_ [type_ "submit", class_ "btn btn-primary"] "Send"

            div_ [class_ "row"] $ do 
                div_ [class_ "col-4 offset-8"] $ do
                    a_ [href_ "/#"] $ "top"
            hr_ []

