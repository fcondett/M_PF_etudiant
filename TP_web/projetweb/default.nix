
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "projetweb" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
