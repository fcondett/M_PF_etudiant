{ pkgs ? import <nixpkgs> {} } :
with pkgs; stdenv.mkDerivation {
    name = "42vainc";
    src = ./.;
    buildInputs = [ pkgconfig gnome3.gtkmm ctags ];
    installPhase = ''
        mkdir -p $out/bin
        cp bin/*.out $out/bin/
    '';
}
