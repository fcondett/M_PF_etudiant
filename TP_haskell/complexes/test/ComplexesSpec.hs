
module ComplexesSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Complexes

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "ComplexesSpec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
