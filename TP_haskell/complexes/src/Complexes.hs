
module Complexes where

type Complexe a = (a, a)

modComp :: Floating a => Complexe a -> a
modComp (a,b) = sqrt ((a * a) + (b * b)) 

conjComp :: Floating a => Complexe a -> a
conjComp (a,b) = a - b

addComp :: Num a => Complexe a -> Complexe a -> Complexe a
addComp (a,b) (a2,b2) = (a + a2,b + b2)   