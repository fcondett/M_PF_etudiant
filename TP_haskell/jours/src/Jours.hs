
module Jours where

data Jour = Lundi | Mardi | Mercredi |
 Jeudi | Vendredi | Samedi | Dimanche
 deriving(Show)

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True 
estWeekend _ = False 

compterOuvrables :: [Jour] -> Int
compterOuvrables = length . filter (not . estWeekend)
