pair x = mod x 2 == 0
estMemeSigne x y = (x <= 0 && y <= 0) || (x > 0 && y > 0)
estTripletPyth x y z = x*x + y*y == z*z
main = do
	print $ estTripletPyth 3 4 5
	print $ estTripletPyth 1 1 34

