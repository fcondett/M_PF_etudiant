
module EvalPartSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import EvalPart

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "EvalPartSpec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
