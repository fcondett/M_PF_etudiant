
module Proj1Spec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Proj1

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "Proj1Spec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
