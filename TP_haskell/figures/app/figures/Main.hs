import Figures
main :: IO ()
main = do
    print $ showFigure (Carre (12::Double))
    print $ showFigure (Rectangle 2 12)
    print $ showFigure (Disque 2)
    print $ calculerAire (Carre 12)
    print $ calculerAire (Rectangle 2 12)
    print $ calculerAire (Disque 2)