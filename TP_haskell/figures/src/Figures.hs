
module Figures where

data Figure a = Carre {cote :: a} | 
    Rectangle {largeur::a, hauteur::a} |
     Disque {rayon::a}
            deriving (Show)


showFigure :: Show a => Figure a -> String
showFigure (Carre c) = "Carre de cote " ++ show c
showFigure (Rectangle w h) = "Rectangle de largeur " ++ show w
    ++ " et de hauteur " ++show h
showFigure (Disque r) = "Disque de rayon " ++ show r

calculerAire :: Floating a => Figure a -> a
calculerAire (Carre c) = c * c
calculerAire (Rectangle w h) = w * h
calculerAire (Disque r) = pi * (r * r)