import Listegens
main :: IO ()
main = do
    let julius = (Kekun "julius" "" "" ["John", "Alan"])
    let sara = (Kekun "sara" "" "" ["John", "Alan"])
    let john = (Kekun "john" "julius" "sara" [""])
    let alan = (Kekun "alan" "julius" "sara" [""])
    print $ showPrenom julius 
    print $ showPere john
    print $ showMere alan
    print $ showEnfants sara