
module Listegens where

data Gengens = Personne | Kekun
    { prenom    :: String
    , pere    :: String
    , mere    :: String
    , enfants   :: [String]
    } deriving (Show)

showPrenom :: Gengens -> String
showPrenom Personne = ""
showPrenom k = show (prenom k)

showPere :: Gengens -> String
showPere Personne = ""
showPere k = show (pere k)

showMere :: Gengens -> String
showMere Personne = ""
showMere k = show (mere k)

showEnfants :: Gengens -> String
showEnfants Personne = ""
showEnfants k = show (enfants k)