
module Listes where

add42 :: Int -> Int
add42 = (+42)

getElem2 = head . tail

rmElem2 xs = head xs : tail(tail xs)
