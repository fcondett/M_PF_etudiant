
module Ds4Spec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Ds4

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "Ds4Spec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
