
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "ds4" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
