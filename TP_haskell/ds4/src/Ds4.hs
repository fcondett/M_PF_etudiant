
module Ds4 where

getPairs1::[(Int,Char)]->[(Int,Char)]
getPairs1 [] = []
getPairs1 (x:xs) = if even (fst x) then x : getPairs1 xs
                    else getPairs1 xs

getPairs2::[(Int,Char)]->[(Int,Char)]
getPairs2 l =  filter (even . fst) l

getPairs3 l = [x | x<-l,even(fst x)]

isSorted1::[Int] -> Bool
isSorted1 [] = True 
isSorted1 [_] = True
isSorted1 (x0:x1:xs) =  
    if x0 > x1 
    then False
    else isSorted1 (x1:xs) 

isSorted2 [] = True 
isSorted2 (x:xs) =
    foldl(\(e0,b) e1 -> (e1,b && e0<e1)) (x,True) xs