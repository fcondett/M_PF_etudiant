
module Lambda where

add42 :: Int -> Int
add42 = (+42)

mul42 = \x ->x * 42

foisTroisPlusUn = (\x -> x + 1).(\x -> 3 * x)