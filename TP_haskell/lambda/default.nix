
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "lambda" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
