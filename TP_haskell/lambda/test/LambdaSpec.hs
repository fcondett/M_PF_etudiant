
module LambdaSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Lambda

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "LambdaSpec" $ do
        it "TODO" $ property $ \x -> add42 x == (x+42)
