import RecapListe
main :: IO ()
main = do 
       print $ doubler1 []
       print $ doubler1 [1,3,8]
       print $ doubler2 []
       print $ doubler2 [1,3,8]
       print $ doubler3 []
       print $ doubler3 [1,3,8]
       print $ taillePaire1 []
       print $ taillePaire1 [4]
       print $ taillePaire1 [1,3,2,4]
       print $ taillePaire3 []
       print $ taillePaire3 [4]
       print $ taillePaire3 [1,3,2,4]
       print $ renverser1 [1,2,3]
       print $ renverser2 [1,2,3]
       print $ renverser3 [1,2,3]