
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "PattMatch" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
