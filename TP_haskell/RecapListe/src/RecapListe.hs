
module RecapListe where

add42 :: Int -> Int
add42 = (+42)

doubler1 :: [Int] -> [Int]
doubler1 [] = []
doubler1 (x:xs) = x*2 : doubler1(xs)

doubler2 :: [Int] -> [Int]
doubler2 l = [2 * x | x<-l]

doubler3 :: [Int] -> [Int]
doubler3 l = map (\x -> 2 * x) l

taillePaire1 :: [Int] -> Bool
taillePaire1 [] = True
taillePaire1 l = (taillePaireAux l 0) `mod` 2 == 0
    where
    taillePaireAux [] cpt = cpt
    taillePaireAux (_:xs) cpt = taillePaireAux xs (cpt+1)
         
taillePaire3 :: [Int] -> Bool
taillePaire3 l = even (length l)

renverser1 :: [Int] -> [Int]
renverser1 [] = []
renverser1 (x:xs) = renverser1(xs) ++ [x]

renverser2 :: [Int] -> [Int]
renverser2 l = foldr(\x xs -> xs ++ [x]) [] l

renverser3 :: [Int] -> [Int]
renverser3 l = foldl(\xs x -> x:xs) [] l