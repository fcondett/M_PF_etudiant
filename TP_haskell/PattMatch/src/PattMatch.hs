
module PattMatch where

add42 :: Int -> Int
add42 = (+42)

null'::[a] -> Bool
null' [] = True 
null' _ = False

head'::[a] -> a
head' [] = error "liste vide"
head' (x:_) = x

tail'::[a] -> [a]
tail' [] = error "liste vide"
tail' (_:xs) = xs

fst'::(a,b) -> a
fst'(x,_) = x