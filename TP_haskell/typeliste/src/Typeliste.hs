
module Typeliste where

data Liste = Nil | Cons Int Liste

afficherListe :: Liste -> String
afficherListe Nil = ""
afficherListe (Cons i l) = show i ++ " " ++ afficherListe l

convertListe :: Liste -> [Int]
convertListe Nil = []
convertListe (Cons i l) = i : convertListe l

sumListe :: Liste -> Int
sumListe Nil = 0
sumListe (Cons i l) = i + sumListe l

data ListeA a = NilA | ConsA a (ListeA a) deriving (Show)

afficherListeA :: Show a => ListeA a -> String
afficherListeA NilA = ""
afficherListeA (ConsA i l) = show i ++ " " ++ afficherListeA l

convertListeA :: ListeA a -> [a]
convertListeA NilA = []
convertListeA (ConsA i l) = i : convertListeA l

sumListeA :: Num a => ListeA a -> a 
sumListeA NilA = 0
sumListeA (ConsA i l) = i + sumListeA l