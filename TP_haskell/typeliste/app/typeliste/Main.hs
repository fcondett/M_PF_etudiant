import Typeliste
main :: IO ()
main = do
    print $ afficherListe (Cons 1 (Cons 2 (Cons 3 Nil)))
    print $ convertListe (Cons 1 (Cons 2 (Cons 3 Nil)))
    print $ sumListe (Cons 1 (Cons 2 (Cons 3 Nil)))

    print $ afficherListeA (ConsA "1" (ConsA "2" (ConsA "3" NilA)))
    print $ convertListeA (ConsA "1" (ConsA "2" (ConsA "3" NilA)))
    print $ sumListeA (ConsA 1 (ConsA 2 (ConsA 3 NilA)))