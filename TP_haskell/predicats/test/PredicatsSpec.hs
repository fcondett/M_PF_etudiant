
module PredicatsSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Predicats

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "PredicatsSpec" $ do
        it "test pair" $ shouldBe (estPair 42) True
        it "test impair" $ shouldBe (estPair 43) False
        it "test estmemesigne" $ shouldBe (estMemeSigne 43 2) True
        it "test tripletpyth" $ shouldBe (estTripletPyth 3 4 5) True
