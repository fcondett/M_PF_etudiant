
module Predicats where

add42 :: Int -> Int
add42 = (+42)

estPair :: Int -> Bool
estPair = even

estMemeSigne :: Int -> Int -> Bool
estMemeSigne x y = x*y > 0

estTripletPyth :: Int -> Int -> Int -> Bool
estTripletPyth x y z = x² + y² == z²