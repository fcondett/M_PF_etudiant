
{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "minmax" ./. {};
in
  if pkgs.lib.inNixShell then drv.env else drv
