
module Minmax where

add42 :: Int -> Int
add42 = (+42)

mini2 :: Int -> Int -> Int
mini2 x y = if x < y then x else y

mini3 :: Int-> Int -> Int -> Int
mini3 x y z = mini2 x (mini2 y z)

minimaxi :: Int -> Int -> (Int,Int)
minimaxi x y = if x < y then (x, y) else (y, x)