
module Contact where

    type Contact = (String, String)     -- (name, email)
    type Base = [Contact]

    search :: Base -> String -> Bool
    search [] nom = False
    search ((n,_):ns) nom = n == nom || search ns nom 
         

    insert :: Base -> Contact -> Base
    insert [] cnt = cnt : []
    insert base cnt =  if search base (fst cnt) 
        then base
        else cnt : base

    update :: Base -> Contact -> Base
    update base cnt = if search base (fst cnt) then map aux base else cnt : base
        where aux c = if fst c == fst cnt
                then cnt
                else c 

    remove :: Base -> String -> Base
    remove base nom = filter (\(n,_) -> n/=nom) base
