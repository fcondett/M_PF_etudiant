import Contact
main :: IO ()
main = do
    print $ search [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] "tutu"
    print $ search [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] "tata"
    print $ insert [("toto","toto@ulco.fr")] ("tata","tata@ulca.fr")
    print $ insert [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] ("tata","tata@nimpe.org")
    print $ update [("toto","toto@ulco.fr")] ("tata","tata@nimpe.org")
    print $ update [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] ("tata","tata@nimpe.org")
    print $ remove [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] "tata"